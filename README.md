# SKILLBOX-APP AWS

This is the code that deploys SKILLBOX-APP on AWS.

![Project](diagrams/img/Project_architecture.png?raw=true "Project architecture")

## Getting started

Start by reading [the getting started guide](docs/guides/getting-started.md).

## Building a new environment

Please see [the environment provisioning guide](docs/guides/environment-provisioning.md) for building a fresh environment.

## Architecture Decision Records

We are recording architecture decisions made to have a history and context of our implementation.

Please see the [ADR documentation](docs/architecture/README.md) for further details.

## Licence

[MIT License](LICENSE)
