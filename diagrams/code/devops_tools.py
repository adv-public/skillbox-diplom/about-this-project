from diagrams import Cluster, Diagram, Edge
from diagrams.aws.analytics import ElasticsearchService, ES
from diagrams.onprem.monitoring import Grafana, Prometheus
from diagrams.onprem.aggregator import Fluentd
from diagrams.onprem.iac import Ansible, Terraform
from diagrams.onprem.vcs import Gitlab, Github, Git
from diagrams.elastic.elasticsearch import Kibana, Elasticsearch
from diagrams.onprem.ci import Gitlabci

# Plan, Code, Build, Test, Release, Deploy, Operate, Monitor

graph_attr = {
    "bgcolor": "transparent"
}

with Diagram("DevOpsTools", graph_attr=graph_attr ,show=False, outformat="png", filename="../img/DevOpsTools"):
    
    with Cluster("Code"):
        Gitlab("Gitlab")
        Github("Github")
        Git("Git")

    with Cluster("Build"):
        Gitlabci("Gitlab CI/CD")

    with Cluster("Monitor"):
        Fluentd("Fluentd")
        Kibana("Kibana")
        ElasticsearchService("ElasticsearchService")
        Grafana("Grafana")
        Prometheus("Prometheus")

    with Cluster("Deploy"):
        Ansible("Ansible")
        Terraform("")

    
