from diagrams import Cluster, Diagram, Edge
from diagrams.aws.compute import EC2, EC2AutoScaling
from diagrams.aws.network import ELB
from diagrams.aws.network import Route53
from diagrams.aws.analytics import ElasticsearchService, ES
from diagrams.onprem.monitoring import Grafana, Prometheus
from diagrams.onprem.aggregator import Fluentd

with Diagram("Network Web Services", show=False, outformat="png", filename="../img/Network Web Services"):
    dns = Route53("denav.click")
    
    with Cluster("VPC-Stage"):

        lb_s = ELB("lb-Stage")

        with Cluster("Public Subnet"):

            with Cluster("AutoScaling"):
                svc_group_s = [EC2("web1"),
                            EC2("web2")]
                
            metrics_s = Prometheus("metric")
            metrics_s << Edge(color="firebrick", style="dashed") << Grafana("monitoring")

    with Cluster("VPC-Prod"):

        lb_p = ELB("lb-Prod")

        with Cluster("Public Subnet"):

            with Cluster("AutoScaling"):
                svc_group_p = [EC2("web1"),
                            EC2("web2")]
                
            metrics_p = Prometheus("metric")
            metrics_p << Edge(color="firebrick", style="dashed") << Grafana("monitoring")

    aggregator = Fluentd("logging")
    aggregator >> Edge(label="parse") \
        >> ES("ElasticsearchService")


    dns >> lb_s >> svc_group_s
    svc_group_s >> aggregator
    svc_group_s >> metrics_s

    dns >> lb_p >> svc_group_p
    svc_group_p >> aggregator
    svc_group_p >> metrics_p
