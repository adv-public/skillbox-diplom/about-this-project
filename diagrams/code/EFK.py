from diagrams import Cluster, Diagram, Edge
from diagrams.aws.compute import EC2, EC2AutoScaling
from diagrams.aws.general import InternetAlt2
from diagrams.aws.network import ELB
from diagrams.aws.network import Route53
from diagrams.aws.analytics import ElasticsearchService, ES
from diagrams.onprem.monitoring import Grafana, Prometheus
from diagrams.onprem.aggregator import Fluentd

with Diagram(name="Elasticsearch + Fluentd + Kibana", show=False, outformat="png", filename="../img/Elasticsearch"):
    
    #internet = InternetAlt2("Internet")
    
    
    with Cluster("VPC"):

        lb_s = ELB("Elastic Load Balancing")

        with Cluster("Public Subnet"):

            with Cluster("AutoScaling"):
                svc_group_s = [EC2("EC2 Instance"),
                            EC2("EC2 Instance")]
                
                aggregator = Fluentd("Fluentd")     
    
    
    es = ES("ElasticsearchService")
    #dns = Route53("denav.click")
    aggregator >> Edge(label="parse") \
        >> es

    #dns >> es
    #internet >> 
    lb_s >> svc_group_s
    svc_group_s >> aggregator
