# 9. AWS VPC architecture
# TODO 
Date: 2022-12-20

## Status

Pending

## Context

The current structure of the `VPC` in the AWS Cloud.

![VPC](./0009-aws-vpc-architecture.png?raw=true "VPC architecture")

## Decision

The current structure of the `VPC` is not optimal, it is necessary to update the structure on the recommendation `Best Practices` [of the AWS](https://docs.aws.amazon.com/quickstart/latest/vpc/architecture.html).

## Consequences

the network settings for the entire infrastructure must be reviewed, which will lead to a possible stop of the service.
