# 8. Variables in Terraform
# TODO
Date: 2022-12-20

## Status

Pending

## Context

[Input variables](https://developer.hashicorp.com/terraform/language/values/variables) let you customize aspects of Terraform modules without altering the module's own source code. This functionality allows you to share modules across different Terraform configurations, making your module composable and reusable.

## Decision

Assign values with a `terraform.tfvars` file
Whenever you execute a plan, destroy, or apply with any variable unassigned, Terraform will prompt you for a value. Entering variable values manually is time consuming and error prone, so Terraform provides several other ways to assign values to variables.

Create a file named terraform.tfvars with the following contents.

```hcl
resource_tags = {
  project     = "new-project",
  environment = "test",
  owner       = "me@example.com"
}

ec2_instance_type = "t3.micro"

instance_count = 3
```

## Consequences

Entering variable values manually is time consuming and error prone.
Rewrite code to use a `terraform.tfvars` file.