# 2. AWS networking outline
# TODO
Date: 2022-12-20

## Status

Pending

## Context

Within AWS, we need to define our IP addressing strategy and assign ranges for different environments.

## Decision

 * 1 VPC per environment (currently integration, staging and production)
 * 1 public subnets, spread across availability zones
 * 1 private subnets, spread across availability zones `(in developing)`


The VPCs will be assigned the following IP ranges:
 
|Environment|IP Range|
|-----------|--------|
|Staging|10.1.0.0/16|
|Production|10.2.0.0/16|

 
Each AZ shall be a `/24` within the above ranges.
 
Placing all the hosts in a private subnet per availability zone and using security groups to maintain our current network isolation.

## Consequences

Private network will be added in the future
