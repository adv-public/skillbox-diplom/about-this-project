# 6. DNS infrastructure
# TODO
Date: 2022-12-20

## Status

Pending

## Context

- Some services and application endpoints will need to be exposed to the Internet and
be resolved by public DNS. For instance grafana, monitoring (Prometheus), test, etc.
- We want to be able to create new pieces of infrastructure alongside the current piece of infrastructure
with the ability to test direct access to each piece using DNS endpoints.
- We want to ensure the site and all links works correctly.

## Decision

![DNS](./0006-dns-infrastructure.png?raw=true "DNS Infrastructure")

#### External Route53 zones

There is a public (external) Route53 zone to manage the external root domain.

## Consequences

Initially we need to manage the entries in the root domain Hetzner.
