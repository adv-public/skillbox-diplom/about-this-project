# Getting Started

Use this guide to help you get started when working on this repository.

# Requirements

## Install necessary packages:

  ```shell
  # for Debian
  apt update
  apt install git ansible -y
  ```

[AWS CLI install and update instructions](https://github.com/aws/aws-cli/tree/v2#installation)


## Create the following directory structure

```
    .
    ├── ...
    ├── backend                  # Deployment services (`skillboxx-app`)
    ├── infrastructure           # Infrastructure building code
    │   ├── ansible              # Using ansible code
    │   └── terraform            # Using terraform code
    │       ├── global           # To set up the base infrastructure
    │       ├── prod             # to set up the prodaction environment
    │       └── stage            # to set up the staging environment
    └── ...
```

## Clone the following repositories to your local machine

1. `terraform\global` [aws-vpc](https://gitlab.com/adv-public/skillbox-diplom/infrastructure/terraform/global/aws-vpc)
1. `terraform\prod` [aws-setup-monitoring-prod](https://gitlab.com/adv-public/skillbox-diplom/infrastructure/terraform/prod/aws-setup-monitoring-prod)
1. `terraform\prod` [aws-web-cluster-prod](https://gitlab.com/adv-public/skillbox-diplom/infrastructure/terraform/prod/aws-web-cluster-prod)
1. `terraform\stage` [aws-setup-efk](https://gitlab.com/adv-public/skillbox-diplom/infrastructure/terraform/stage/aws-setup-efk)
1. `terraform\stage` [aws-setup-monitoring-stage](https://gitlab.com/adv-public/skillbox-diplom/infrastructure/terraform/stage/aws-setup-monitoring-stage)
1. `terraform\stage` [aws-web-cluster-stage](https://gitlab.com/adv-public/skillbox-diplom/infrastructure/terraform/stage/aws-web-cluster-stage)
1. `terraform` [modules](https://gitlab.com/adv-public/skillbox-diplom/infrastructure/terraform/modules)

### Or use `ghorg` to quickly clone all of an repos into a single directory.
https://github.com/gabrie30/ghorg

Clone a specific group, preserving the directory structure of subgroups

```bash
ghorg clone adv-public/skillbox-diplom --scm=gitlab --base-url=https://gitlab.com --preserve-dir --token=XXX
```


> **NOTE: Ensure Ansible has all dependencies installed**
>
> If this step is not completed you may see errors when deploying about
> missing functions
>

# Deploying code

Please see the SKILLBOX-APP Developer Docs to [Deploy AWS infrastructure with Terraform](/docs/guides/deploying-terraform.md)