# Environment Provisioning

This document discusses how to create a new environment in AWS. Discussion of the decisions made can be found in the [docs/architecture/decisions](/docs/architecture/decisions) directory, with particular note is [the environment bootstrapping process](/docs/architecture/decisions/0004-environment-bootstrapping-process.md).

To clarify terms used here, there is a [glossary](##glossary). Throughout this document, `<foo>` indicates a value you supply (e.g. a stack name) and:

```
bar
```
indicates a command to be run from a shell.

## Overview

The general steps for provisioning a new environment are:

1. [Create the following directory structure](/docs/guides/getting-started.md#create-the-following-directory-structure)
2. [Clone all the relevant repositories](/docs/guides/getting-started.md#clone-the-following-repositories-to-your-local-machine)
3. [Provision the base infrastructure](#provision-the-base-infrastructure)
4. [Deploy the core applications](#deploy-the-core-applications-skillbox-app)


## Glossary

This explains how these terms are used in this document.


## Provision the base infrastructure

Several Terraform projects need to be run to set up the base infrastructure. For each of these, you should run plan and apply in the build script.

The projects that need to be initially run in this way are:

1. `aws-vpc` (configured `backend local`)
2. `aws-setup-efk`
3. `aws-web-cluster-stage` (run 2 times `apply` to get ip addresses, configured `backend local`)
4. `aws-web-cluster-prod` (run 2 times `apply` to get ip addresses, configured `backend local`)
5. `aws-setup-efk` (to update the white list of web-cluster addresses from `backend local`)
6. `aws-setup-monitoring-stage`
7. `aws-setup-monitoring-prod`

## Deploy the core applications SKILLBOX-APP

### Setup with ansible pipeline

Use the received IP addresses from the web cluster and the GitLab runner for deployment.

Go to the [install-gitlab-runner](https://gitlab.com/adv-public/skillbox-diplom/infrastructure/ansible/install-gitlab-runner) repository and run the playbook according to the [instructions](https://gitlab.com/adv-public/skillbox-diplom/infrastructure/ansible/install-gitlab-runner/-/blob/main/README.md).

### Then, you need to run the pipeline in GitLab

Pipelines can be manually executed with predefined or manually-specified variables.

[Run a pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually).

The pipeline now executes the jobs as configured in [gitlab-ci.yml](https://gitlab.com/adv-public/skillbox-diplom/backend/service-skillbox-app/-/blob/main/.gitlab-ci.yml).
