# Deploy Terraform

We use [Terraform](https://terraform.io/) to configure the SKILLBOX-APP infrastructure in AWS.

# One-time setup

1. Check that you have sufficient access
The changes you can deploy depend on the role you have assumed in our AWS environments.


  * People with production access can assume 'poweruser' roles, which can deploy anything except IAM.
  * People with production access can assume 'administrator' roles, which can deploy anything.

You should always use the least privileged role that will let you accomplish your task.

Unless your terraform plan needs to make changes to IAM resources, use a poweruser role.

2. Install aws-cli

[AWS CLI install and update instructions](https://github.com/aws/aws-cli/tree/v2#installation)

3. Configuring the AWS CLI

[Configuration basics](https://github.com/aws/aws-cli/tree/v2#getting-started)
